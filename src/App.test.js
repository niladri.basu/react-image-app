import React from 'react';
import App from './App';
import {shallow, mount} from 'enzyme';


describe('App', () => {

  let app;
  let cards;

  beforeEach(() => {
    app = shallow(<App />);
    cards = [
      {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
      {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
      {name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
  ];
  })

  it('renders without crashing', () => {
    shallow(<App />);
  });
  
  it('renders header', () => {
    app = shallow(<App />).find('header');
    expect(app.length).toBe(1);
  });

  //render CardList on App
  it('should render CardList on App', () => {

    expect(app.find('CardList').length).toBe(1);
    // app.instan
    // expect(app.find('.card-details').length).toBe(3);

  })

  it('should render 3 cards', () => {
    app = mount(<App/>);
    app.setState({cards: cards});
    expect(app.find('.card-details').length).toBe(3);
  })


})
