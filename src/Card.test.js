import React from 'react'
import { shallow } from 'enzyme'
import Card from './Card'

describe('Card', () => {
    let card;
    let props;
    beforeEach(() => {

       props = {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"};
        card = shallow(<Card {...props}/>)
    })

    it('should render Card', () => {
        expect(card);
    })

    it('should return parent div with css class', () =>{
        const parentDiv = card.find('.card-details');
        expect(parentDiv.length).toBe(1);
    })

    it('should render a image in card.', () => {
        //Validating img value
        expect(card.find('img').prop('src')).toEqual(props.avatar_url);

    })

    it('should render info div fro image', () => {
        //Validating img value
        expect(card.find('.info').length).toBe(1);
    })

    it('should render name and company in ifo div', () =>{
        let infoDetail = card.find('.info');

        //validate name is rendered
        expect(infoDetail.find('.name').text()).toEqual(props.name);

        // Validate Company Name is rendered
        expect(infoDetail.find('.company').text()).toEqual(props.company);


    })
})