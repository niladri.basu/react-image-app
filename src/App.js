import React, { useState } from 'react';
import './App.css';
import CardList from './CardList';
class App extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      cards: []
    }
  }
  render () {
    return (
      <div className="App">
        <header className="App-header">
            <CardList cards = {this.state.cards}/>
        </header>
      </div>
    );
  }
}

export default App;
