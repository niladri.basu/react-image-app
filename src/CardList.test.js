import React from 'react';
import { shallow } from 'enzyme';
import CardList from './CardList';

describe('CardList', () => {
    let cardList;
    let cards;
    beforeEach(() => {
        cards = [
            {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
            {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
            {name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
        ];
        cardList = shallow(<CardList cards = {cards}/>)
    })

    it('should render cardList', () => {
        expect(cardList.find('div').length).toBe(1);
    });

    it('should render card list with all cards', () => {
        expect(cardList.find('div').children().length).toBe(cards.length);
    })
})
