import React from 'react';
import './Card.css'

function Card (props) {
    return (
        <div className="card-details">
            <img src={props.avatar_url} />
            <div className="info">
                <div className="name">{props.name}</div>
                <div className="company">{props.company}</div>
            </div>
        </div>
    );
}

export default Card;

/*
const testData = [
    {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
    {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
    {name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
];
*/