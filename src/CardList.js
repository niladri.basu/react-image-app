import React from 'react';
import Card from './Card';
export default function CardList (props) {
    return (
        <div>
            {props.cards.map(profile => <Card {...profile} key={profile.name}/>)}
        </div>
    );
}